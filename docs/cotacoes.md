# Cotações

## Provedores

| Provedor  |          500MB           |          600MB           |          900MB           |
| :-------- | :----------------------: | :----------------------: | :----------------------: |
| **Core3** |            -             | R$ 110<br>^0,18^ ^R$/MB^ |            -             |
| **GNET**  | R$ 130<br>^0,26^ ^R$/MB^ |            -             | R$ 200<br>^0,23^ ^R$/MB^ |

## Equipamentos

|          Modelo          |                   Amazon                    |                FastShop                 | Magalu                                 |
| :----------------------: | :-----------------------------------------: | :-------------------------------------: | -------------------------------------- |
| TP-Link Deco X20<br>1 un |                                             |                                         |                                        |
| TP-Link Deco X20<br>2 un |   530,25 [^amz12]<br>~10x106,13=1.060,49~   | 909,42 [^fsp12]<br>~12x101,07=1.212,83~ |                                        |
| TP-Link Deco X20<br>3 un |   743,11 [^amz13]<br>~10x222,93=2.229,33~   |                                         |                                        |
| TP-Link Deco X50<br>1 un |                                             |                                         |                                        |
| TP-Link Deco X50<br>2 un | ~~630,65~~ [^amz22]<br>~10x126,21=1.261,29~ | 594,95 [^fsp22]<br>~10x118,99=1.189,90~ | 623,15 [^mlu22]<br>~10x124,63=1246,30~ |
| TP-Link Deco X50<br>3 un |                                             |                                         |                                        |
|     TP-link TL-SG108     |                                             |                                         |                                        |

[^amz12]: [TP-LINK Deco X20(2- pack) Wi-fi 6 em Toda a Casa Mesh AX1800](https://www.amazon.com.br/X20-2-PACK-AX1800-WHOLE-SYSTEM/dp/B07G392VCD/){: target="_blank" }
[^fsp12]: [Kit Roteador MESH Wi-Fi 6, Deco X50, AX3000, TP-Link, Gigabit, 2 Unidades](https://www.fastshop.com.br/web/p/d/YTDECOX502BCO_PRD/roteador-wireless-deco-x50-ax3000-tp-link---02-unidades---decox502-ytdecox502bcoprd){: target="_blank" }
[^amz13]: [TP-Link Deco X20 WiFi 6 AX1800 (3-Pack) AX1800 Sistema Wi-Fi 6 em Toda a Casa Mesh](https://www.amazon.com.br/TP-Link-Deco-X20-AX1800-Sistema/dp/B084KJF5DJ/){: target="_blank" }
[^amz22]: [Kit Roteador Mesh Wi-Fi 6 Gigabit AX3000 - Deco X50(2-pack)(US)](https://www.amazon.com.br/Roteador-Mesh-Wi-Fi-Gigabit-AX3000/dp/B09LVFNMVJ/){: target="_blank" }
[^fsp22]: [Kit Roteador MESH Wi-Fi 6, Deco X50, AX3000, TP-Link, Gigabit, 2 Unidades](https://www.fastshop.com.br/web/p/d/YTDECOX502BCO_PRD/kit-roteador-mesh-wi-fi-6-deco-x50-ax3000-tp-link-gigabit-2-unidades){: target="_blank" }
[^mlu22]: [Kit Roteador MESH Wi-Fi 6, Deco X50, AX3000, TP-Link, Gigabit, 2 Unidades](https://www.magazineluiza.com.br/kit-roteador-mesh-wi-fi-6-deco-x50-ax3000-tp-link-gigabit-2-unidades/p/kc3fb1e6a1/in/rtdr/){: target="_blank" }
[^kbm3]: [Switch 8 Portas TP-link 10/100/1000 Gigabit TL-SG108](https://www.kabum.com.br/produto/61837/switch-8-portas-tp-link-10-100-1000-gigabit-tl-sg108){: target="_blank" }
