# Projeto

## Esboço

![Diagrama](assets/wifi-mangueiras.png)

## Quantitativo

|  #  | Item                | Unidade | Condomínio |  1  |  2  |  3  |  4  |  6  |  7  | Total |
| :-: | ------------------- | ------- | :--------: | :-: | :-: | :-: | :-: | :-: | :-: | :---: |
|  1  | Access Point        | Un      |     0      |  1  |  1  |  1  |  2  |  1  |  2  |   8   |
|  2  | Switch 8 portas     | Un      |     1      |  -  |  -  |  -  |  -  |  -  |  -  |   1   |
|  3  | Cabo UTP Cat6       | Cx      |     1      |  -  |  -  |  -  |  -  |  -  |  -  |   1   |
|  4  | Conector Cat6 macho | Un      |     30     |  -  |  -  |  -  |  -  |  -  |  -  |  30   |

## Requisitos

### Wifi

1. :material-wifi::material-numeric-6-circle: Wi-Fi 6 (802.11ax)
2. :material-antenna: Mesh (802.11k/v/r)
3. :octicons-number-16: Suporte a 7 unidades
4. :material-virus-outline: Antivírus
5. :simple-letsencrypt: Criptografia WPA3
6. :material-wifi: Rede para IoT
   Esse tipo de dispositivo costuma funcionar em baixas velocidades, degradando o desempenho da rede como um todo. A existência de uma rede exclusiva para eles permite que se obtenha o melhor desempenho da rede principal.
7. :material-cable-data: Backhaul ethernet
8. :material-wrench: Garantia nacional

### Switch

1. Gigabit Ethernet 802.3ab
2. Gerenciável
3. Controle de Fluxo 802.3X
4. QoS 802.1P/DSCP
5. IGMP Snooping
6. Fanless
